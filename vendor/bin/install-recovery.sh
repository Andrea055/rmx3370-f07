#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:134217728:37402d487f200f01c727a23621dd1dcb6b265f5e; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:100663296:3d1adc1d404e6d2eabe254f2571699d5eb99e831 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:134217728:37402d487f200f01c727a23621dd1dcb6b265f5e && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
